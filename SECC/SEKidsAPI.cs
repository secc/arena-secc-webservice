﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel.Web;
using Arena.Security;
using Arena.Custom.HDC.WebService.Contracts;
using Arena.Services.Contracts;
using Arena.Event;
using Arena.Services.Exceptions;
using System.Text.RegularExpressions;
using Arena.Core;
using System.Linq;
using Arena.Custom.SECC.Common.Util;
using Arena.Organization;
using System.Data.SqlClient;
using Arena.Core.Communications;

namespace Arena.Custom.HDC.WebService.SECC
{
    class SEKidsAPI
    {
        
        // Default to online giving
        private string currentModule = "SEKids.Preregistration";

        /// <summary>
        /// <b>POST person/SEKidsPreRegistion</b>
        ///
        /// Submit person's Online Starting Point data
        /// </summary>
        /// <returns>ModifyResult.</returns>
        [WebInvoke(Method = "POST",
                UriTemplate = "sekids/preregister")]
        [RestApiAnonymous]
        public Arena.Services.Contracts.ModifyResult SEKidsPreRegister(Contracts.SEKidsPreRegister sekpr)
        {
            ModifyResult result = new ModifyResult();

            // Return if we have a null request
            if (sekpr == null)
            {
                result.Successful = false.ToString();
                result.ValidationResults.Add(new ModifyValidationResult() { Key = "NullRequest", Message = "Null request or unable to serialize." });
                result.Link = null;
                return result;
            }

            // Setup some defaults
            result.Successful = true.ToString();
            result.Link = "/sekids/preregister/familyid/null";

            // Get all the phone number types
            LookupCollection lc = new LookupCollection();
            lc.LoadByType(38);
            String activeTypes = lc.Where(e => e.Active).Select(e => e.Value).Aggregate((current, next) => current + ", " + next);

            // First Name
            foreach (Contracts.SEKidsPreRegister.FamilyMember fm in sekpr.FamilyMembers)
            {
                if (String.IsNullOrEmpty(fm.FirstName))
                {
                    result.Successful = false.ToString();
                    result.ValidationResults.Add(new ModifyValidationResult() { Key = "FirstNameMissing", Message = "First Name is required." });
                    result.Link = null;
                }

                // Last Name
                if (String.IsNullOrEmpty(fm.LastName))
                {
                    result.Successful = false.ToString();
                    result.ValidationResults.Add(new ModifyValidationResult() { Key = "LastNameMissing", Message = "Last Name is required." });
                    result.Link = null;
                }

                // Gender
                if (fm.Gender == Arena.Custom.HDC.WebService.Contracts.SEKidsPreRegister.Gender.Undefined)
                {
                    result.Successful = false.ToString();
                    result.ValidationResults.Add(new ModifyValidationResult() { Key = "GenderMissing", Message = "Gender is required." });
                    result.Link = null;
                }

                // DOB
                if (fm.DateOfBirth == null || fm.DateOfBirth == default(DateTime))
                {
                    result.Successful = false.ToString();
                    result.ValidationResults.Add(new ModifyValidationResult() { Key = "DOBMissingOrInvalid", Message = "Date of Birth required and must be valid." });
                    result.Link = null;

                    // If the DOB is not valid, don't do any more checks on this person
                    continue;
                }

                // This person is an child, make sure we have a grade
                if (fm.Age < 18)
                {
                    if (fm.Age >= 6 && fm.Grade == null)
                    {
                        result.Successful = false.ToString();
                        result.ValidationResults.Add(new ModifyValidationResult() { Key = "GradeMissing", Message = "Grade is required for all famly members between the ages of 6 and 18." });
                        result.Link = null;
                    }
                    continue;
                }

                // Email
                if (!String.IsNullOrEmpty(fm.Email))
                {
                    if (!Regex.Match(fm.Email, @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$").Success)
                    {
                        result.Successful = false.ToString();
                        result.ValidationResults.Add(new ModifyValidationResult() { Key = "EmailInvalid", Message = "Email format is invalid: person@secc.org" });
                        result.Link = null;
                    }
                }


                // Phone Number
                if (String.IsNullOrEmpty(fm.PhoneNumber))
                {
                    result.Successful = false.ToString();
                    result.ValidationResults.Add(new ModifyValidationResult() { Key = "PhoneNumberMissing", Message = "Phone Number is required." });
                    result.Link = null;
                }
                else
                {
                    // Match the phone number format
                    if (!Regex.Match(fm.PhoneNumber, @"^(\([2-9]\d\d\)|[2-9]\d\d) ?[-.,]? ?[2-9]\d\d ?[-.,]? ?\d{4}$").Success)
                    {
                        result.Successful = false.ToString();
                        result.ValidationResults.Add(new ModifyValidationResult() { Key = "PhoneNumberInvalid", Message = "Phone Number format is invalid: (502) 253-8000" });
                        result.Link = null;
                    }
                }


                // Phone Type
                Arena.Core.Lookup phoneType = new Arena.Core.Lookup();
                if (String.IsNullOrEmpty(fm.PhoneType))
                {
                    result.Successful = false.ToString();
                    result.ValidationResults.Add(new ModifyValidationResult() { Key = "PhoneTypeInvalid", Message = "Phone type is invalid (" + activeTypes + ")" });
                    result.Link = null;
                }
                else
                {
                    phoneType = lc.Where(e => e.Value == fm.PhoneType).FirstOrDefault();

                    if (phoneType == null)
                    {
                        result.Successful = false.ToString();
                        result.ValidationResults.Add(new ModifyValidationResult() { Key = "PhoneTypeInvalid", Message = "Phone type is invalid (" + activeTypes + ")" });
                        result.Link = null;
                    }
                }

                // Validate the campus
                if (String.IsNullOrEmpty(fm.Campus))
                {
                    result.Successful = false.ToString();
                    result.ValidationResults.Add(new ModifyValidationResult() { Key = "CampusInvalid", Message = "Campus is required." });
                    result.Link = null;
                }
                else
                {
                    // Clean up the campus name
                    fm.Campus = fm.Campus.Replace("Campus", "").Trim();
                }
            }


            if (result.Successful == true.ToString())
            {

                Int32? familyId = null;
                foreach (Contracts.SEKidsPreRegister.FamilyMember fm in sekpr.FamilyMembers.Where(fm => fm.DateOfBirth < DateTime.Now.AddYears(-1)))
                {

                    // If this is an adult
                    if (fm.Age >= 18) { 
                        PersonMatch pm = new PersonMatch();
                        pm.FirstName = fm.FirstName.Trim();
                        pm.LastName = fm.LastName.Trim();
                        pm.BirthDate = fm.DateOfBirth;
                        if (!String.IsNullOrEmpty(fm.PhoneNumber))
                        {
                            pm.Phone = fm.PhoneNumber.Trim();
                        }
                        if (!String.IsNullOrEmpty(fm.Email))
                        {
                            pm.Email = fm.Email.Trim();
                        }
                        Arena.Core.Lookup phoneType = lc.Where(e => e.Value == fm.PhoneType).FirstOrDefault();

                        if (phoneType != null)
                        {
                            pm.PhoneType = phoneType.Guid;
                        }

                        // Make sure every record is active
                        pm.RecordStatus = Arena.Enums.RecordStatus.Active;

                        List<Arena.Core.Person> people = pm.GetMatches();
                        if (people.Count == 1)
                        {
                            Arena.Core.Person person = people.FirstOrDefault();
                            fm.PersonID = person.PersonID;
                            familyId = person.FamilyId;
                        }
                    }
                }
                
                // Get organizational settings
                OrganizationSettingCollection orgSettings = new OrganizationSettingCollection(ArenaContext.Current.Organization.OrganizationID);

                Int32 tagId = 0;
                try
                {
                    tagId = Int32.Parse(orgSettings.FindByKey("SEKidsPreregistrationTagId").Value);
                }
                catch (Exception)
                {
                    throw new Exceptions.ArenaApplicationException("Unable to load Starting Point organization settings.  Please review all the settings.");
                }

                // Load a default person because we use this a few times.  The defaultPerson is any adult in the household that will populate 
                // phone number/email and campus for any children in the household as well as being the primary contact
                SEKidsPreRegister.FamilyMember defaultPerson = sekpr.FamilyMembers.Where(fm => fm.Age >= 18).FirstOrDefault();

                // If we have a person match for all adults, and they are in the same family we'll use that family
                if (sekpr.FamilyMembers.Where(fm => fm.Age >= 18).All(fm => fm.PersonID >= 0 && new Arena.Core.Person(fm.PersonID).FamilyId == familyId))
                {
                    foreach(SEKidsPreRegister.FamilyMember fm in sekpr.FamilyMembers)
                    {
                        Arena.Core.Person person = null;
                        if (fm.PersonID <= 0)
                        {
                            person = CreatePerson(fm, familyId, defaultPerson);
                        }
                        else
                        {
                            person = new Arena.Core.Person(fm.PersonID);
                            PersonMatch.syncEmailPhone(fm.Email, fm.PhoneNumber, lc.Where(e => e.Value == (fm.PhoneType)).FirstOrDefault(), person);
                            SyncAllergiesAndDisabilities(fm, person);
                        }

                        if (person != null && person.Age <= 18)
                        {
                            // Push them into the preregistration tag
                            AddToTag(person, tagId, sekpr);
                        }
                    }
                    result.Link = "/sekids/preregister/familyid/"+familyId;
                }
                else
                {
                    foreach (Contracts.SEKidsPreRegister.FamilyMember fm in sekpr.FamilyMembers.Where(fm => fm.DateOfBirth < DateTime.Now.AddYears(-1)))
                    {
                        
                        Arena.Core.Person person = CreatePerson(fm, familyId, defaultPerson);
                        if (!familyId.HasValue)
                        {
                            familyId = person.FamilyId;
                        }

                        if (person.Age <= 18)
                        {
                            // Push them into the preregistration tag
                            AddToTag(person, tagId, sekpr);
                        }
                    }

                    result.Link = "/sekids/preregister/familyid/" + familyId;
                }

                // Now email the person!
                SqlDataReader reader = new Arena.DataLayer.Core.CommunicationTemplateData().GetCommunicationTemplates(ArenaContext.Current.Organization.OrganizationID);

                // All this complicated stuff is to look for an email template by name
                Int32 templateId = 0;
                Int32 templateIdOther = 0;
                while (reader.Read())
                {
                    Int32 tempId = 0;
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        if (reader.GetName(i) == "template_id")
                        {
                            tempId = reader.GetInt32(i);
                            break;
                        }
                    }
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        if (reader.GetName(i) == "template_name" && reader.GetString(i).Contains("SEKids Preregistration - " + defaultPerson.Campus))
                        {
                            templateId = tempId;
                            break;
                        }
                        if (reader.GetName(i) == "template_name" && reader.GetString(i).Contains("SEKids Preregistration - Other"))
                        {
                            templateIdOther = tempId;
                            break;
                        }
                    }
                }

                // If the email template exists, email the person
                if (templateId > 0 || templateIdOther > 0)
                {

                    // Fetch the email template and populate it
                    AdvancedHTML email = new AdvancedHTML();
                    email.TemplateID = templateId>0?templateId:templateIdOther;

                    Dictionary<string, string> fields = new Dictionary<string, string>();
                    fields.Add("##ParentFirstName##", defaultPerson.FirstName);
                    fields.Add("##ParentLastName##", defaultPerson.LastName);
                    fields.Add("##ParentPhone##", defaultPerson.PhoneNumber);
                    fields.Add("##Campus##", defaultPerson.Campus);

                    string[] kids = sekpr.FamilyMembers.Where(fm => fm.Age < 18).Select(fm => fm.FirstName).ToArray();
                    string kidsNames = string.Join(", ", kids.Take(kids.Length - 1)) + (kids.Length <= 1 ? "" : " and ") + kids.LastOrDefault();

                    fields.Add("##KidsNames##", kidsNames);

                    email.Send(defaultPerson.Email, fields);
                }


            }

            return result;

        }

        /// <summary>
        /// Add a person to an arena tag
        /// </summary>
        /// <param name="person">The Person</param>
        /// <param name="tagId">The tag id</param>
        /// <param name="sekpr">The request payload for this webservice</param>
        private void AddToTag(Arena.Core.Person person, int tagId, SEKidsPreRegister sekpr) {
        
            // Push them into the preregistration tag
            Arena.Core.ProfileMember tag = new Arena.Core.ProfileMember();
            tag.PersonID = person.PersonID;
            tag.ProfileID = tagId;
            tag.MemberNotes = person.Campus.Name + ": " + sekpr.ServiceTime + (!String.IsNullOrEmpty(sekpr.Notes!=null?sekpr.Notes.Trim():"") ? " - Other Notes:" + sekpr.Notes : "");
            tag.Save(currentModule);
        }

        /// <summary>
        /// Create a person in Arena
        /// </summary>
        /// <param name="fm">The family member</param>
        /// <param name="familyId">The family id</param>
        /// <param name="defaultPerson">The family member which has default email/phone/campus data</param>
        /// <returns>The newly created (or found) person</returns>
        private Arena.Core.Person CreatePerson(SEKidsPreRegister.FamilyMember fm, Int32? familyId, SEKidsPreRegister.FamilyMember defaultPerson = null)
        {
            // Load the phone number types collection
            LookupCollection lc = new LookupCollection();
            lc.LoadByType(38);

            PersonMatch pm = new PersonMatch();
            pm.FirstName = fm.FirstName;
            pm.LastName = fm.LastName;
            pm.BirthDate = fm.DateOfBirth;
            pm.HomePhone = fm.PhoneNumber ?? defaultPerson.PhoneNumber;
            pm.PhoneType = lc.Where(e => e.Value == (fm.PhoneType ?? defaultPerson.PhoneType)).Select(l => l.Guid).FirstOrDefault();
            pm.Email = fm.Email ?? defaultPerson.Email;
            if (familyId.HasValue)
            {
                pm.FamilyId = familyId.Value;
            }
            pm.RecordStatus = Arena.Enums.RecordStatus.Active;
            pm.CurrentModule = "SEKids.Preregistration";

            String campus = String.IsNullOrEmpty(fm.Campus)?defaultPerson.Campus:fm.Campus;

            if (!string.IsNullOrEmpty(campus))
            { 
                Arena.Organization.CampusCollection cc = new Arena.Organization.CampusCollection(Arena.Core.ArenaContext.Current.Organization.OrganizationID);
                Arena.Organization.Campus campusObj = cc.Where(c => c.Name.ToLower().Contains(campus.ToLower())).FirstOrDefault();
                // If this is an actual campus, go ahead and set it
                if (campusObj != null)
                {
                    pm.Campus = campusObj.CampusId;
                }
            }
            Arena.Core.Person person = pm.MatchOrCreateArenaPerson();
            person.Gender = (Arena.Enums.Gender)fm.Gender;
            if (!String.IsNullOrEmpty(fm.Grade))
            {
                int gradeInt;
                if (Int32.TryParse(fm.Grade, out gradeInt)) {
                    person.GraduationDate = Arena.Core.Person.CalculateGraduationYear(gradeInt, ArenaContext.Current.Organization.GradePromotionDate);
                }
            }
            person.Save(1, currentModule, true);
            SyncAllergiesAndDisabilities(fm, person);

            return person;
        }

        private void SyncAllergiesAndDisabilities (SEKidsPreRegister.FamilyMember fm, Arena.Core.Person person) {
            if (!String.IsNullOrEmpty(fm.Allergies))
            {
                Arena.Core.PersonAttribute pa = new Arena.Core.PersonAttribute(person.PersonID, 80);
                pa.StringValue = pa.HasStringValue ? pa.StringValue + " - " + fm.Allergies : fm.Allergies;
                pa.Save(ArenaContext.Current.Organization.OrganizationID, currentModule);
            }
            if (!String.IsNullOrEmpty(fm.Disabilities))
            {
                Arena.Core.PersonAttribute pa = new Arena.Core.PersonAttribute(person.PersonID, 83);
                pa.StringValue = pa.HasStringValue ? pa.StringValue + " - " + fm.Disabilities : fm.Disabilities;
                pa.Save(ArenaContext.Current.Organization.OrganizationID, currentModule);
            }
        }

    }
}
