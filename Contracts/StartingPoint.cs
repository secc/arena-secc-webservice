﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arena.Custom.HDC.WebService.Contracts
{
    [DataContract(Namespace = "")]
    public class StartingPoint : PersonMatchData
    {
        [DataMember(EmitDefaultValue = false)]
        public Boolean DesiresToBecomeMember { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Boolean AgreesWithSoF { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Boolean WatchedVideos { get; set; }
    }
}
