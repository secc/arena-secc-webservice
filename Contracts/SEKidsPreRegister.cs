﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace Arena.Custom.HDC.WebService.Contracts
{

    [DataContract(Namespace = "")]
    public class SEKidsPreRegister
    {

        public enum Gender
        {
            Undefined = -1,
            Male = 0,
            Female = 1,
            Unknown = 2,
        }

        [XmlArray("FamilyMembers", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [XmlArrayItem("FamilyMember", typeof(FamilyMember), Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DataMember(EmitDefaultValue = false, Name="FamilyMembers")]
        public Family FamilyMembers = new Family();

        [DataMember(EmitDefaultValue = false)]
        public String Notes;

        [DataMember(EmitDefaultValue = false)]
        public String ServiceTime;

        [CollectionDataContract(ItemName="FamilyMember", Namespace="")]
        public class Family : System.Collections.Generic.List<FamilyMember> {}

        [DataContract(Namespace = "")]
        public class FamilyMember : PersonMatchData
        {
            /// <summary>
            /// Get the person's age
            /// </summary>
            public Int32 Age
            {
                get
                {
                    int num;
                    try
                    {
                        if (!(DateOfBirth != DateTime.MinValue) || !(DateOfBirth != DateTime.Parse("1/1/1900")))
                        {
                            num = -1;
                        }
                        else
                        {
                            DateTime now = DateTime.Now;
                            int year = 0;
                            int month = 0;
                            if (now.Day - DateOfBirth.Day < 0)
                            {
                                month--;
                            }
                            month = month + (now.Month - DateOfBirth.Month);
                            if (month < 0)
                            {
                                year--;
                            }
                            year = year + (now.Year - DateOfBirth.Year);
                            num = year;
                        }
                    }
                    catch
                    {
                        num = -1;
                    }
                    return num;
                }
            }

            private Gender _gender = Gender.Undefined;
            [DataMember(EmitDefaultValue = true)]
            public Gender Gender
            {
                get{
                    return _gender;
                }
                set
                {
                    _gender = (Gender)Enum.Parse(typeof(Gender), value.ToString(), true);
                }
            }

            [DataMember(EmitDefaultValue = false)]
            public String Allergies { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public String Disabilities { get; set; }

            [DataMember(EmitDefaultValue = true)]
            public String Grade { get; set; }
        }
    }
    
}
